import React from 'react';
import {Button, Dropdown, DropdownButton} from "react-bootstrap";

export default class Formulari extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tasca: ''
        }
        this.enviaForm = this.enviaForm.bind(this);
        this.canviTasca = this.canviTasca.bind(this);
    }
    canviTasca(event) {
        this.setState({ tasca: event.target.value });
    }

    enviaForm(event) {
        event.preventDefault();

        this.props.novaTasca(this.state.tasca);
        this.setState({
            tasca: ''
        })
    }

    render() {
        let i = 1;
        return (
            <form onSubmit={this.enviaForm}>
                <div className="form-group">
                    <label>Tasca</label>
                    <input type="text" value={this.state.tasca} className="form-control" onChange={this.canviTasca} />
                </div>
                <DropdownButton id="dropdown-basic-button" title="Prioridad">
                    <Button type="submit" className="btn btn-primary" id="primary">Trabajo</Button>
                    <Button type="submit" className="btn btn-success" id="success">Personal</Button>
                    <Button type="submit" className="btn btn-danger" id="danger">Urgente</Button>
                    <Button type="submit" className="btn btn-warning" id="warning">Familia</Button>
                </DropdownButton>
             
            </form>


        );
    }
}


