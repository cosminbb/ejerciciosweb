import React from "react";  
import Todolist from './Todolist';

export default () => (
  <>
    <Todolist />
  </>
);