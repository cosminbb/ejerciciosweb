import React from "react";
import HolaMundo from "./HolaMundo";
import Bola from "./Bola";
import Cuadrado from "./Cuadrado";
import Separador from "./Separador";

export default () => (
  <>
  <HolaMundo />
  <Bola />
  <Cuadrado />
  <Separador />
  </>
);
