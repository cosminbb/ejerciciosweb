class Calculadora extends React.Component {
        
    constructor(props){
        super(props);
        //this state debe contener las variables necesarias para
        //dar soporte a las operaciones
        //pantalla contiene el contenido del "display" de la calculadora
        this.state = {
            pantalla: 0
            //otras variables... operacion? última tecla? valor_anterior?
        }
        this.clicado = this.clicado.bind(this);   
    }

    // el método clicado recibirá el texto de la tecla que ha sido pulsada
    // y deberá realizar las operaciones

    clicado(tecla){

        const nums =  [0,1,2,3,4,5,6,7,8,9];
        const ops = ["*","+","-","/"];

        if (tecla == "C") {
            this.setState({
                //borramos contenido de la pantalla
                pantalla: 0
            }); 
        } else
        //tecla es un número
        if (nums.indexOf(tecla)>-1){
            this.setState({
               // concatenar tecla al contenido de la pantalla actual
            });
        } else
        //tecla es una operación
        if (ops.indexOf(tecla)>-1) {
            this.setState({
              // guardamos valor en pantalla y también operación en curso
            });
        } else {
            // tecla es =
            // recuperamos valor guardado y operación en curso,
            // realizamos operación con valor actual en pantalla
            // y mostramos el resultado...

        } 
    }


  render() {
      return(
        <div>
            <div className="pantalla">
                <div className="resultado">{this.state.pantalla}</div>
            </div>
            <div className="teclado">  
                <Fila teclas={[1,2,3,"*"]} onClick={this.clicado} />
                <Fila teclas={[4,5,6,"/"]} onClick={this.clicado} />
                <Fila teclas={[7,8,9,"-"]} onClick={this.clicado} />
                <Fila teclas={[0,"C","=","+"]} onClick={this.clicado} />
            </div>
        </div>
      );
  }


}