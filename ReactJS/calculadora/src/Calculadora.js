import React from 'react';
import "./calculadora.css";
import Fila from "./Fila.js";

export default class Calculadora extends React.Component {
    constructor(props) {
        super(props);
        this.state = { resultado: 0 };
        this.changeResultado =

            this.changeResultado.bind(this);
        this.clicado = this.clicado.bind(this);
    }

    changeResultado() {
        this.setState({
            resultado: this.state.resultado
        })
    }

    clicado(tecla) {
        const nums = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        const ops = ["*", "+", "-", "/"];

        if (tecla == "C") {
            this.setState({
                pantalla: 0
            })
        } else if (indexOf(tecla) > -1) {
            this.setState()
        }



    }

    render() {
        return (
            <div>
                <div className="pantalla">
                    <div className="resultado">{this.state.pantalla}</div>
                </div>
                <div className="teclado">
                    <button className="tecla">1</button>
                    <button className="tecla">2</button>
                    <button className="tecla">3</button>
                    <button className="tecla">4</button>
                    <button className="tecla">5</button>
                    <button className="tecla">6</button>
                    <button className="tecla">7</button>
                    <button className="tecla">8</button>
                    <button className="tecla">9</button>

                    <button className="tecla">0</button>
                    <button className="tecla">C</button>
                    <button className="tecla">=</button>
                    <button className="tecla">+</button>

                    <Fila teclas={[1, 2, 3, "*"]} onClick={this.clicado} />
                    <Fila teclas={[4, 5, 6, "/"]} onClick={this.clicado} />
                    <Fila teclas={[7, 8, 9, "-"]} onClick={this.clicado} />
                    <Fila teclas={[0, "C", "=", "+"]} onClick={this.clicado} />
                </div>
            </div>
        )
    }
}
