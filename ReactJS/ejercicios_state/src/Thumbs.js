import React from "react";
import { faThumbsUp, faThumbsDown, faIcons } from '@fortawesome/free-solid-svg-icons';


export default class Thumbs extends React.Component {
    constructor(props) {
        super(props);
        this.state = { Thumbs: false };
        this.changeAlert =

            this.changeAlert.bind(this);

    }
    changeAlert() {
        this.setState({
            Thumbs: !this.state.Thumbs
        })
    }
    render() {
        
        const theClass =

            (this.state.Thumbs) ? "ThumbsUp" : "ThumbsDown";

        return (
            
            <div onClick={this.changeAlert}
                className={theClass} faicons={faThumbsUp}></div>

        )
    }
}