import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col } from 'reactstrap';

import Home from './Home';
import Pagina1 from './Pagina1';
import Pagina2 from './Pagina2';
import Pagina3 from './Pagina3';
import Pagina4 from './Pagina4';

export default () => (
    <BrowserRouter>
        <Container>

            <Row>
                <Col>
                    <ul>
                        <li> <Link to="/">Home</Link> </li>
                        <li> <Link to="/pagina1">Pagina1</Link> </li>
                        <li> <Link to="/pagina2">Pagina2</Link> </li>
                        <li> <Link to="/pagina3">Pagina3</Link> </li>
                        <li> <Link to="/pagina4">Pagina4</Link> </li>
                    </ul>
                </Col>
            </Row>

            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/pagina1" component={Pagina1} />
                <Route path="/pagina2" component={Pagina2} />
                <Route path="/pagina3" component={Pagina3} />
                <Route path="/pagina4" component={Pagina4} />
                <Route component={NotFound} />
            </Switch>

        </Container>
    </BrowserRouter>
);
